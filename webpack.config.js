const path = require( 'path' );
const webpack = require( 'webpack' );
const { VueLoaderPlugin } = require( 'vue-loader' );
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');

const paths = {
    src: __dirname + '/src',
    dist: __dirname + '/web',
    public: `/${process.env.PUBLIC_PATH || ''}/`.replace('//', '/'),
}

module.exports = {
	context: paths.src,
	plugins: [
		new VueLoaderPlugin(),
		new HtmlWebpackPlugin({
            title: process.env.APP_TITLE,
            filename: 'index.html',
            template: path.join(process.cwd(), 'public/index.ejs'),
		}),
		new CopyWebpackPlugin([
			{
				from: paths.src+'/assets/',
				to: paths.dist+'/assets/'
			},
			{
				from: paths.src+'/public/favicon.ico',
				to: paths.dist+'/favicon.ico'
			},
		]),
	],
	mode: 'development',
	entry: {
        app: `${paths.src}/main.js`,
    },
	output: {
		filename: '[name].js',
		path: paths.dist,
        publicPath: paths.public,
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						'scss': 'vue-style-loader!css-loader!sass-loader',
						'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
					}
				}
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'file-loader',
				
				options: {
					name: '[name].[ext]',
					path: paths.dist,
        			publicPath: paths.public,
				}
			},
			{
				test: /\.css$/,
				use: [
					'vue-style-loader',
					'css-loader'
				]
			},
			{
				test: /\.(sass|scss)$/,
				use: [
					'vue-style-loader',
					'css-loader',
					'sass-loader'
				]
			}
		],
		
	},
	resolve: {
		alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@modules': path.resolve( 'src/modules' ),
			'@components': path.resolve( 'src/components' ),
			'@pages': path.resolve( 'src/pages' ),
			'@mixins': path.resolve( 'src/mixins' ),
		}
	},
	devServer: {
		historyApiFallback: {index: paths.public},
		noInfo: true,
		host: 'localhost',
        port: '8001',
		hot: true,
		contentBase: paths.dist,
	},
	performance: {
		hints: false
	},
	devtool: '#eval-source-map',
};

if ( process.env.NODE_ENV === 'production' ) {
	module.exports.devtool = 'source-map';
	module.exports.mode = 'production';
}
