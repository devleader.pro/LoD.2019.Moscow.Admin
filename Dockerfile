FROM node:latest as builder
RUN mkdir -p /app
COPY ./ ./app
WORKDIR /app 
RUN npm i
RUN npm run build

FROM nginx
RUN mkdir -p /app
WORKDIR /app 
COPY --from=builder /app/web ./web
RUN sed -i 's#/usr/share/nginx/html#/app/web/#g'  /etc/nginx/conf.d/default.conf
RUN service nginx start
CMD ["nginx", "-g", "daemon off;"]
