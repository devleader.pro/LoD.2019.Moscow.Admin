export default {
	name: 'page',
	props: {
		title: String,
		required: true,
		date: Boolean
	},
	data: function() {
		return {
			data: [],
			dataReady: false
		};
	},
	
	watch: {
		'$route'( to, from ) {
		}
	}
};
