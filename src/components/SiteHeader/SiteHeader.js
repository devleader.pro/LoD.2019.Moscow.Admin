export default {
	name: 'page',
	mixins: [  ],
	components: {
		
	},
	mounted() {
	},
	props: {
		
	},
	data: function() {
		return {
			data: [],
			dataReady: false
		};
	},
	methods: {
		logout(){
			this.$root.token = null
			localStorage.removeItem('token')
			
		}
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
