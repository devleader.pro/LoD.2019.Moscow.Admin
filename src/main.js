//import Plugins
import "babel-polyfill";
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { HTTP as axios } from './axios/axios';
Vue.prototype.$http = axios;
// register global

Vue.use( Vuex );
Vue.use( VueRouter );
Vue.config.debug = true;
Vue.config.devTools = true;

import App from './App.vue';
Vue.component( 'App', App );
import Users from '@pages/Users/Users.vue';
Vue.component( 'Users', Users );

import Tasks from '@pages/Tasks/Tasks.vue';
Vue.component( 'Tasks', Tasks );
import TaskAdd from '@pages/TaskAdd/TaskAdd.vue';
Vue.component( 'TaskAdd', TaskAdd );
import Task from '@pages/Task/Task.vue';
Vue.component( 'Task', Task );
import Profile from '@pages/Profile/Profile.vue';
Vue.component( 'Profile', Profile );
import Templates from '@pages/Templates/Templates.vue';
Vue.component( 'Templates', Templates );
import Template from '@pages/Template/Template.vue';
Vue.component( 'Template', Template );
import Wizard from '@pages/Wizard/Wizard.vue';
Vue.component( 'Wizard', Wizard );
import Auth from '@pages/Auth/Auth.vue';
Vue.component( 'Auth', Auth );
// import Templates from '@pages/Templates/Templates.vue';
// Vue.component( 'Task', Task );
//Routers
const router = new VueRouter( {
	mode: 'history',
	routes: [
		{ path: '/Auth',  component: Auth }, 
		{ path: '/tasks', component: Tasks }, 	
		{ path: '/tasks/:id',  component: Task }, 	
		{ path: '/tasks/add', component: TaskAdd},
		{ path: '/', redirect: '/tasks' }, 	
		{ path: '/users', component: Users },
		{ path: '/users/:id', component: Profile },
		
		{ path: '/templates/', component: Templates },
		// { path: '/templates/:id', component: Template },
		{ path: '/templates/add', component: Wizard },
		{ path: '*', redirect: '/tasks' }
		
	]
} );

const store = new Vuex.Store( {
	state: {
		title: ''
	},
	mutations: {
		rtChangeTitle( state, value ) {
			state.title = value;
			document.title = ( state.title ? state.title + ' - ' : '' ) + 'RootLead - автомизатор рутины';
		}
	}
} );

new Vue( {
	router,
	store,
	render: createElement => createElement( App ),
	data() {
		return {
			token: null,
			// uid: '',
			// me: {},
			// meReady:true,
			// templates: []
		};
	},
	methods: {
		getUid(){
			this.token = window.localStorage.token
			this.auth = true;
		},
		getToken(){
			// if(window.localStorage.token){
			// 	this.$http.get(`User/uid/${window.localStorage.token}`).then((res)=>{
			// 		this.getUid();
			// 		this.uid = res.data.id
			// 		this.me = res.data
			// 	}).catch((e) => {
			// 		window.localStorage.token = undefined
			// 		this.token = ''
			// 		this.auth = false
			// 		this.uid = null
			// 	})
				
			// } else {
			// 	window.localStorage.token = undefined
			// 	this.token = ''
			// 	this.auth = false
			// 	this.uid = null
			// }
		},
		
	}
} ).$mount( '#app' );
