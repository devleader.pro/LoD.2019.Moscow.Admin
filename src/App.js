import SiteHeader from '@components/SiteHeader/SiteHeader.vue'

export default {
	mounted() {
		this.$root.getToken()
		this.getToken()
	},
	components: { 
		SiteHeader,
	},
	mixins: [ 
	],
	data() {
		return {
			login: '',
			password: '',
			token: '',
			uid: ''

		};
	},
	
	methods: {
		getToken(){
			console.log(window.localStorage.token, this.$route.path)
			if(!window.localStorage.token){
				this.$router.push({path: '/auth'})
			} else if (window.localStorage.token && this.$route.path === '/auth'){
				this.$router.push({path: '/tasks'})
				return
			} 
			this.$root.token = window.localStorage.token

			

		},
		getAuth(){
			this.$http.get( '/Auth', {
				params: {
					login: this.login,
					password: this.password,
				}
			} ).then( ( res ) => {
				this.token = res.data.token
				this.$root.uid = res.data.uid
				this.$root.auth = true
				window.localStorage.token = res.data.token
				this.$root.getToken()
			} );
		}
	
	},
	watch: {
		'$route'( from, to ) {
			
		},
		["$root.token"](){
			this.getToken()
		}
	}
};
