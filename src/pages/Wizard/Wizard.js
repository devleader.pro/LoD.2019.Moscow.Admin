import SitePanel from '@components/SitePanel/SitePanel.vue'

export default {
	name: 'page',
	components: {
		SitePanel
	},
	data: function() {
		return {
			siteurl: '',
			page: `
			<body id="jira" class="aui-layout aui-theme-default page-type-dashboard" data-version="8.2.4" data-aui-version="8.0.3">
			<div id="page">
			<header id="header" role="banner">
			
			
			
			
			
			
			
			<script>
			require(["jquery", "jira/license-banner"], function ($, licenseBanner) {
			$(function () {
			licenseBanner.showLicenseBanner("");
			licenseBanner.showLicenseFlag("");
			});
			});
			</script>
			
			
			
			
			
			
			
			
			
			
			
			<nav class="aui-header aui-dropdown2-trigger-group" role="navigation" resolved="" data-aui-responsive="true"><div class="aui-header-inner"><div class="aui-header-primary"><h1 id="logo" class="aui-header-logo aui-header-logo-custom"><a href="https://board.devlead.pro/secure/MyJiraHome.jspa"><img src="/s/-6hucr7/802003/4460b9f1b01702e29f2e6191917088ae/_/jira-logo-scaled.png" alt="" data-aui-responsive-header-index="0"><span class="aui-header-logo-text">Board</span></a></h1><ul class="aui-nav" style="width: auto;"><li><a href="/secure/Dashboard.jspa" class=" aui-nav-link aui-dropdown2-trigger aui-dropdown2-ajax" id="home_link" aria-haspopup="true" aria-controls="home_link-content" title="Просмотр и управление Вашими рабочими столами" accesskey="D" resolved="" aria-expanded="false">Рабочий стол</a><div class="aui-dropdown2 aui-style-default aui-layer" id="home_link-content" data-aui-dropdown2-ajax-key="home_link" resolved="" aria-hidden="true"></div></li>
			</ul></div><div class="aui-header-secondary"><ul class="aui-nav">
			<li id="quicksearch-menu">
			<form action="/secure/QuickSearch.jspa" method="get" id="quicksearch" class="aui-quicksearch dont-default-focus ajs-dirty-warning-exempt">
			<input id="quickSearchInput" autocomplete="off" class="search" type="text" title="Поиск ( Нажмите '/' )" placeholder="Поиск" name="searchString" accesskey="Q"><div class="quick-search-spinner"></div>
			<input type="submit" class="hidden" value="Search">
			</form>
			</li>
			<li><a class="jira-feedback-plugin" role="button" aria-haspopup="true" id="jira-header-feedback-link" href="#"><span class="aui-icon aui-icon-small aui-iconfont-feedback">Отправить отзыв в Atlassian</span></a></li>
			
			
			
			
			<li id="system-help-menu">
			<a class="aui-nav-link aui-dropdown2-trigger aui-dropdown2-trigger-arrowless" id="help_menu" aria-haspopup="true" href="https://docs.atlassian.com/jira/jcore-docs-082/" target="$textUtils.htmlEncode($rootHelpMenuItem.params.target)" title="Справка" resolved="" aria-controls="system-help-menu-content" aria-expanded="false"><span class="aui-icon aui-icon-small aui-iconfont-question-filled">Справка</span></a>
			<div id="system-help-menu-content" class="aui-dropdown2 aui-style-default aui-layer" resolved="" aria-hidden="true">
			<div class="aui-dropdown2-section">
			<ul id="jira-help" class="aui-list-truncate">
			<li>
			<a id="view_core_help" class="aui-nav-link " title="Перейти к онлайн документации на ПО Jira Core" href="https://docs.atlassian.com/jira/jcore-docs-082/" target="_blank">Справка Jira Core</a>
			</li>
			<li>
			<a id="keyshortscuthelp" class="aui-nav-link " title="Получить больше информации о Горящих клавишах Jira ( Нажмите '?' )" href="/secure/ViewKeyboardShortcuts!default.jspa" target="_blank">Горячие клавиши</a>
			</li>
			<li>
			<a id="view_about" class="aui-nav-link " title="Получить больше информации о Jira" href="/secure/AboutPage.jspa">О Jira</a>
			</li>
			<li>
			<a id="view_credits" class="aui-nav-link " title="Показать, кто это сделал" href="/secure/credits/AroundTheWorld!default.jspa" target="_blank">Список участников Jira</a>
			</li>
			</ul>
			</div>
			</div>
			</li>
			
			
			
			
			
			
			
			
			
			
			<li id="user-options">
			<a class="aui-nav-link login-link" href="/login.jsp?os_destination=%2Fdefault.jsp">Вход</a>
			<div id="user-options-content" class="aui-dropdown2 aui-style-default aui-layer" resolved="" aria-hidden="true">
			<div class="aui-dropdown2-section">
			</div>
			</div>
			</li>
			</ul></div></div><!-- .aui-header-inner--></nav><!-- .aui-header -->
			</header>
			
			
			
			
			<section id="content" role="main">
			<dashboard params="{&quot;dashboardDirectoryResourceUrl&quot;:&quot;http://board.devlead.pro/rest/config/1.0/directory&quot;,&quot;dashboardUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000&quot;,&quot;dashboardDirectoryUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0//directory/10000&quot;,&quot;layoutAction&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000/layout&quot;,&quot;errorGadgetUrl&quot;:&quot;http://board.devlead.pro/plugins/servlet/gadgets/error/spec-not-found&quot;,&quot;writable&quot;:false,&quot;canAddExternalGadgetsToDirectory&quot;:false,&quot;dashboardDiagnosticsUrl&quot;:&quot;/plugins/servlet/gadgets/dashboard-diagnostics&quot;,&quot;maxGadgets&quot;:20,&quot;gadgetDirectoryAdminUrl&quot;:&quot;http://board.devlead.pro/plugins/servlet/gadgets/admin&quot;,&quot;dashboardDirectoryBaseUrl&quot;:&quot;http://board.devlead.pro/&quot;,&quot;securityTokensUrl&quot;:&quot;http://board.devlead.pro/plugins/servlet/gadgets/security-tokens&quot;,&quot;dashboardResourceUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000&quot;}" layouts="{&quot;layouts&quot;:[{&quot;layout&quot;:&quot;AA&quot;,&quot;resourceUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000&quot;,&quot;active&quot;:true,&quot;id&quot;:&quot;10000&quot;,&quot;title&quot;:&quot;System Dashboard&quot;,&quot;uri&quot;:&quot;Dashboard.jspa?selectPageId=10000&quot;,&quot;gadgets&quot;:[{&quot;hasNonHiddenUserPrefs&quot;:false,&quot;color&quot;:&quot;color1&quot;,&quot;column&quot;:0,&quot;colorUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/10000/color&quot;,&quot;title&quot;:&quot;Информация&quot;,&quot;userPrefs&quot;:{&quot;action&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/10000/prefs&quot;,&quot;fields&quot;:[]},&quot;loaded&quot;:true,&quot;gadgetUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/10000&quot;,&quot;width&quot;:0,&quot;context&quot;:{&quot;view&quot;:{&quot;viewType&quot;:{&quot;aliases&quot;:[&quot;DEFAULT&quot;,&quot;DASHBOARD&quot;,&quot;profile&quot;,&quot;home&quot;],&quot;name&quot;:&quot;default&quot;,&quot;canonicalName&quot;:&quot;default&quot;}},&quot;dashboardItem&quot;:{&quot;id&quot;:&quot;10000&quot;,&quot;properties&quot;:{}},&quot;dashboard&quot;:{&quot;id&quot;:&quot;10000&quot;}},&quot;isMaximizable&quot;:true,&quot;id&quot;:&quot;10000&quot;,&quot;configurable&quot;:false,&quot;height&quot;:0,&quot;inlineHtml&quot;:&quot;<div class=\&quot;g-intro\&quot;><div class=\&quot;intro\&quot;><img class=\&quot;intro-logo\&quot; src=\&quot;/images/intro-illustration.png\&quot;/><h3>Добро пожаловать в Board<\/h3><p>Новичок в Jira? Ознакомьтесь с <a id=\&quot;user-docs\&quot; href=\&quot;https://docs.atlassian.com/jira/jcore-docs-082/\&quot; alt=\&quot;Get help!\&quot; title=\&quot;Documentation\&quot;> Руководством пользователя Jira <\/a>.<\/p><\/div><\/div>&quot;},{&quot;hasNonHiddenUserPrefs&quot;:false,&quot;color&quot;:&quot;color1&quot;,&quot;column&quot;:1,&quot;colorUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/0/color&quot;,&quot;amdModule&quot;:&quot;jira-dashboard-items/login&quot;,&quot;title&quot;:&quot;Вход в систему&quot;,&quot;userPrefs&quot;:{&quot;action&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/0/prefs&quot;,&quot;fields&quot;:[{&quot;name&quot;:&quot;isElevatedSecurityCheckShown&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;false&quot;,&quot;required&quot;:false},{&quot;name&quot;:&quot;isAdminFormOn&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;false&quot;,&quot;required&quot;:false},{&quot;name&quot;:&quot;loginFailedByPermissions&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;false&quot;,&quot;required&quot;:false},{&quot;name&quot;:&quot;externalUserManagement&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;false&quot;,&quot;required&quot;:false},{&quot;name&quot;:&quot;isPublicMode&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;false&quot;,&quot;required&quot;:false},{&quot;name&quot;:&quot;allowCookies&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;true&quot;,&quot;required&quot;:false},{&quot;name&quot;:&quot;captchaFailure&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;false&quot;,&quot;required&quot;:false},{&quot;name&quot;:&quot;loginSucceeded&quot;,&quot;type&quot;:&quot;string&quot;,&quot;value&quot;:&quot;false&quot;,&quot;required&quot;:false}]},&quot;loaded&quot;:true,&quot;gadgetUrl&quot;:&quot;http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/0&quot;,&quot;width&quot;:0,&quot;context&quot;:{&quot;view&quot;:{&quot;viewType&quot;:{&quot;aliases&quot;:[&quot;DEFAULT&quot;,&quot;DASHBOARD&quot;,&quot;profile&quot;,&quot;home&quot;],&quot;name&quot;:&quot;default&quot;,&quot;canonicalName&quot;:&quot;default&quot;}},&quot;dashboardItem&quot;:{&quot;id&quot;:&quot;0&quot;,&quot;properties&quot;:{&quot;isElevatedSecurityCheckShown&quot;:&quot;false&quot;,&quot;isAdminFormOn&quot;:&quot;false&quot;,&quot;loginFailedByPermissions&quot;:&quot;false&quot;,&quot;externalUserManagement&quot;:&quot;false&quot;,&quot;isPublicMode&quot;:&quot;false&quot;,&quot;allowCookies&quot;:&quot;true&quot;,&quot;captchaFailure&quot;:&quot;false&quot;,&quot;loginSucceeded&quot;:&quot;false&quot;}},&quot;dashboard&quot;:{&quot;id&quot;:&quot;10000&quot;}},&quot;isMaximizable&quot;:true,&quot;id&quot;:&quot;0&quot;,&quot;configurable&quot;:false,&quot;height&quot;:0,&quot;inlineHtml&quot;:&quot;<div id=\&quot;login-container\&quot; class=\&quot;\&quot;><form id=\&quot;loginform\&quot; method=\&quot;post\&quot; action=\&quot;#\&quot; name=\&quot;loginform\&quot; class=\&quot;aui gdt\&quot;><div class=\&quot;field-group\&quot;><label accesskey=\&quot;u\&quot; for=\&quot;login-form-username\&quot; id=\&quot;usernamelabel\&quot;><u>И<\/u>мя пользователя<\/label><input class=\&quot;text medium-field\&quot; id=\&quot;login-form-username\&quot; name=\&quot;os_username\&quot; type=\&quot;text\&quot;><\/div><div class=\&quot;field-group\&quot;><label accesskey=\&quot;p\&quot; for=\&quot;login-form-password\&quot; id=\&quot;passwordlabel\&quot;><u>П<\/u>ароль<\/label><input class=\&quot;text medium-field\&quot; id=\&quot;login-form-password\&quot; name=\&quot;os_password\&quot; type=\&quot;password\&quot;><\/div><fieldset class=\&quot;group\&quot;><div class=\&quot;checkbox\&quot; id=\&quot;rememberme\&quot;><input class=\&quot;checkbox\&quot; id=\&quot;login-form-remember-me\&quot; name=\&quot;os_cookie\&quot; type=\&quot;checkbox\&quot; value=\&quot;true\&quot;><label accesskey=\&quot;r\&quot; for=\&quot;login-form-remember-me\&quot; id=\&quot;remembermelabel\&quot;><u>З<\/u>апомнить меня<\/label><\/div><\/fieldset><div class=\&quot;field-group\&quot; id=\&quot;publicmodeooff\&quot;><div id=\&quot;publicmodeoffmsg\&quot;>Еще не зарегистрированы? Чтобы получить аккаунт, Пожалуйста, обратитесь к администраторам <a id=\&quot;contact-admin\&quot; href=\&quot;/secure/ContactAdministrators!default.jspa\&quot;>Вашей<\/a> Jira.<\/div><\/div><div class=\&quot;buttons-container\&quot;><div class=\&quot;buttons\&quot;><input class=\&quot;aui-button aui-button-primary\&quot; id=\&quot;login\&quot; name=\&quot;login\&quot; type=\&quot;submit\&quot; value=\&quot;Войти\&quot;><a class=\&quot;cancel\&quot; href=\&quot;/secure/ForgotLoginDetails.jspa\&quot; id=\&quot;forgotpassword\&quot; target=\&quot;_parent\&quot;>Не можете попасть в систему?<\/a><\/div><\/div><\/form><\/div>&quot;}],&quot;writable&quot;:false}]}" resolved=""><div id="dashboard" class="dashboard"><div id="dashboard-content"><div class="aui-page-header"><div class="aui-page-header-inner"><div class="aui-page-header-main"><h1>System Dashboard</h1></div><div class="aui-page-header-actions" id="dash-options"><div id="dash-throbber" class="throbber">&nbsp;</div><a aria-haspopup="true" class="aui-button aui-dropdown2-trigger aui-dropdown2-trigger-arrowless" resolved="" aria-controls="tools-dropdown-items" aria-expanded="false" href="#tools-dropdown-items"><span class="aui-icon aui-icon-small aui-iconfont-more">Настройки</span></a><div id="tools-dropdown-items" class="aui-style-default aui-dropdown2 aui-layer" resolved="" aria-hidden="true"><ul class="aui-list-truncate"><li><a id="wallboard_link" class="item-link groupmarker" href="http://board.devlead.pro/plugins/servlet/Wallboard/?dashboardId=10000">Просмотреть в виде панели</a></li></ul></div></div></div></div><div class="dashboard-shim"> </div><div class="layout layout-aa"><ul class="column first" style="min-height: 317px;"><li class="gadget" id="rep-10000" style="height: 158px;"></li></ul><ul class="column second" style="min-height: 317px;"><li class="gadget" id="rep-0" style="height: 316px;"></li></ul><ul class="column third" style="min-height: 317px;"></ul></div><div class="gadget color1" id="gadget-10000-renderbox" style="left: 3.93939%; top: 70px; width: 45.3535%;"><div class="dashboard-item-frame gadget-container" id="gadget-10000-chrome"><div class="dashboard-item-header"><h3 id="gadget-10000-title" class="dashboard-item-title">Информация</h3><div class="gadget-menu"><ul><li><a href="#" id="gadget-10000-maximize" class="maximize" title="Увеличить"><span class="aui-icon aui-icon-small aui-iconfont-vid-full-screen-on"></span></a></li><li class="aui-dd-parent dd-allocated"><a id="undefined" class="aui-dd-trigger standard" href="#"><span class="aui-icon-small aui-iconfont-more"></span></a><ul class="aui-dropdown standard hidden"><li class="dropdown-item"><a class="item-link minimization" href="#">Уменьшить</a></li></ul></li></ul></div></div><div class="dashboard-item-content "><form action="http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/10000/prefs" class="aui userpref-form hidden" method="post" id="gadget-10000-edit"><fieldset><legend><span class="dashboard-item-title">Информация</span> Свойства</legend></fieldset><div class="buttons-container submit"><input type="submit" class="submit" value="Сохранить" name="save" id="gadget-10000-save"><input type="reset" class="cancel" value="Отмена" name="reset" id="gadget-10000-cancel"></div></form><div id="gadget-10000" class="gadget-inline" style="height: 128px;"><div class="g-intro"><div class="intro"><img class="intro-logo" src="/images/intro-illustration.png"><h3>Добро пожаловать в Board</h3><p>Новичок в Jira? Ознакомьтесь с <a id="user-docs" href="https://docs.atlassian.com/jira/jcore-docs-082/" alt="Get help!" title="Documentation"> Руководством пользователя Jira </a>.</p></div></div></div></div></div></div><div class="gadget color1" id="gadget-0-renderbox" style="left: 51.4394%; top: 70px; width: 45.3535%;"><div class="dashboard-item-frame gadget-container gadget-hover" id="gadget-0-chrome"><div class="dashboard-item-header"><h3 id="gadget-0-title" class="dashboard-item-title">Вход в систему</h3><div class="gadget-menu"><ul><li><a href="#" id="gadget-0-maximize" class="maximize" title="Увеличить"><span class="aui-icon aui-icon-small aui-iconfont-vid-full-screen-on"></span></a></li><li class="aui-dd-parent dd-allocated"><a id="undefined" class="aui-dd-trigger standard" href="#"><span class="aui-icon-small aui-iconfont-more"></span></a><ul class="aui-dropdown standard hidden"><li class="dropdown-item"><a class="item-link minimization" href="#">Уменьшить</a></li></ul></li></ul></div></div><div class="dashboard-item-content "><form action="http://board.devlead.pro/rest/dashboards/1.0/10000/gadget/0/prefs" class="aui userpref-form hidden" method="post" id="gadget-0-edit"><fieldset><legend><span class="dashboard-item-title">Вход в систему</span> Свойства</legend><div class="field-group"><label for="gadget-0-form-isElevatedSecurityCheckShown-pref">undefined</label><input id="gadget-0-form-isElevatedSecurityCheckShown-pref" type="text" value="false" name="up_isElevatedSecurityCheckShown" class="string text"><br></div><div class="field-group"><label for="gadget-0-form-isAdminFormOn-pref">undefined</label><input id="gadget-0-form-isAdminFormOn-pref" type="text" value="false" name="up_isAdminFormOn" class="string text"><br></div><div class="field-group"><label for="gadget-0-form-loginFailedByPermissions-pref">undefined</label><input id="gadget-0-form-loginFailedByPermissions-pref" type="text" value="false" name="up_loginFailedByPermissions" class="string text"><br></div><div class="field-group"><label for="gadget-0-form-externalUserManagement-pref">undefined</label><input id="gadget-0-form-externalUserManagement-pref" type="text" value="false" name="up_externalUserManagement" class="string text"><br></div><div class="field-group"><label for="gadget-0-form-isPublicMode-pref">undefined</label><input id="gadget-0-form-isPublicMode-pref" type="text" value="false" name="up_isPublicMode" class="string text"><br></div><div class="field-group"><label for="gadget-0-form-allowCookies-pref">undefined</label><input id="gadget-0-form-allowCookies-pref" type="text" value="true" name="up_allowCookies" class="string text"><br></div><div class="field-group"><label for="gadget-0-form-captchaFailure-pref">undefined</label><input id="gadget-0-form-captchaFailure-pref" type="text" value="false" name="up_captchaFailure" class="string text"><br></div><div class="field-group"><label for="gadget-0-form-loginSucceeded-pref">undefined</label><input id="gadget-0-form-loginSucceeded-pref" type="text" value="false" name="up_loginSucceeded" class="string text"><br></div></fieldset><div class="buttons-container submit"><input type="submit" class="submit" value="Сохранить" name="save" id="gadget-0-save"><input type="reset" class="cancel" value="Отмена" name="reset" id="gadget-0-cancel"></div></form><div id="gadget-0" class="gadget-inline" style="height: 286px;"><div id="login-container" class=""><form id="loginform" method="post" action="#" name="loginform" class="aui gdt top-label"><div class="field-group"><label accesskey="u" for="login-form-username" id="usernamelabel"><u>И</u>мя пользователя</label><input class="text medium-field" id="login-form-username" name="os_username" type="text"></div><div class="field-group"><label accesskey="p" for="login-form-password" id="passwordlabel"><u>П</u>ароль</label><input class="text medium-field" id="login-form-password" name="os_password" type="password"></div><fieldset class="group"><div class="checkbox" id="rememberme"><input class="checkbox" id="login-form-remember-me" name="os_cookie" type="checkbox" value="true"><label accesskey="r" for="login-form-remember-me" id="remembermelabel"><u>З</u>апомнить меня</label></div></fieldset><div class="field-group" id="publicmodeooff"><div id="publicmodeoffmsg">Еще не зарегистрированы? Чтобы получить аккаунт, Пожалуйста, обратитесь к администраторам <a id="contact-admin" href="/secure/ContactAdministrators!default.jspa">Вашей</a> Jira.</div></div><div class="buttons-container"><div class="buttons"><input class="aui-button aui-button-primary" id="login" name="login" type="submit" value="Войти" resolved=""><a class="cancel" href="/secure/ForgotLoginDetails.jspa" id="forgotpassword" target="_parent">Не можете попасть в систему?</a></div></div></form></div></div></div></div></div></div></div><div id="diagnostic-warning" class="hidden"></div><div id="i18n-settings"><input class="locale-lang" type="hidden" value="ru"><input class="locale-country" type="hidden" value="RU"></div><dashboard-done resolved=""></dashboard-done></dashboard>
			</section>
			<footer id="footer" role="contentinfo">
			
			
			<section class="footer-body">
			<ul class="atlassian-footer">
			<li>
			Atlassian Jira <a class="seo-link" rel="nofollow" href="https://www.atlassian.com/software/jira">Project Management Software</a>
			<span id="footer-build-information">(v8.2.4#802003-<span title="300fef2d82ba5ac49e806212aa28fbdd3470d0ae" data-commit-id="300fef2d82ba5ac49e806212aa28fbdd3470d0ae}">sha1:300fef2</span>)</span>
			</li>
			<li>
			<a id="about-link" rel="nofollow" href="/secure/AboutPage.jspa/secure/AboutPage.jspa">About Jira</a>
			</li>
			<li>
			<a id="footer-report-problem-link" rel="nofollow" href="/secure/CreateIssue!default.jspa">Report a problem</a>
			</li>
			</ul>
			<ul class="atlassian-footer">
			<li class="licensemessagered">
			При поддержке бесплатной <a rel="nofollow" href="http://www.atlassian.com/software/jira">временной лицензии Jira</a> Atlassian. Пожалуйста, подумайте <a rel="nofollow" href="/plugins/servlet/applications/versions-licenses">о ее покупке</a> сегодня.
			
			</li>
			</ul>
			
			<div id="footer-logo"><a rel="nofollow" href="http://www.atlassian.com/">Atlassian</a></div>
			</section>
			
			
			
			
			
			
			
			
			
			
			
			<fieldset class="hidden parameters">
			<input type="hidden" title="loggedInUser" value="">
			<input type="hidden" title="ajaxTimeout" value="Вызов сервера Jira не был завершен в течение тайм-аута. Мы не уверены в результате этой операции.">
			<input type="hidden" title="JiraVersion" value="8.2.4">
			<input type="hidden" title="ajaxUnauthorised" value="Вы не авторизованы для выполнения этой операции. Пожалуйста, войдите.">
			<input type="hidden" title="baseURL" value="http://board.devlead.pro">
			<input type="hidden" title="ajaxCommsError" value="Невозможно связаться с сервером Jira. Это может быть временный сбой или сервер недоступен. ">
			<input type="hidden" title="ajaxServerError" value="Сервер Jira доступен, но вернул сообщение об ошибке. Мы не уверены в результате этой операции.">
			<input type="hidden" title="ajaxErrorCloseDialog" value="Закройте это диалоговое окно и нажмите обновление в браузере">
			<input type="hidden" title="ajaxErrorDialogHeading" value="Разрыв связи">
			
			<input type="hidden" title="dirtyMessage" value="Вы ввели новую информацию на этой странице. Если вы покинете страницу, предварительно не сохранив изменения, они будут потеряны.">
			<input type="hidden" title="dirtyDialogMessage" value="Вы ввели в это окно новые данные. Если вы уйдете из этого окна, не сохранив данные, все изменения будут потеряны. Чтобы вернуться к окну, нажмите «отменить».">
			<input type="hidden" title="keyType" value="Нажмите">
			<input type="hidden" title="keyThen" value="затем">
			<input type="hidden" title="dblClickToExpand" value="Двойное нажатие для раскрытия">
			<input type="hidden" title="actions" value="Действия">
			<input type="hidden" title="removeItem" value="Удалить">
			<input type="hidden" title="workflow" value="Бизнес-процесс">
			<input type="hidden" title="labelNew" value="Новая метка">
			<input type="hidden" title="issueActionsHint" value="Начните набирать для поиска доступных операций или нажмите вниз, чтобы увидеть все">
			<input type="hidden" title="closelink" value="Закрыть">
			<input type="hidden" title="dotOperations" value="Операции">
			<input type="hidden" title="dotLoading" value="Загрузка">
			<input type="hidden" title="frotherSuggestions" value="Предложения">
			<input type="hidden" title="frotherNomatches" value="Нет совпадений">
			<input type="hidden" title="multiselectVersionsError" value="{0} не является корректной версией.">
			<input type="hidden" title="multiselectComponentsError" value="{0} - некорректный компонент.">
			<input type="hidden" title="multiselectGenericError" value="Некорректное значение {0}.">
			</fieldset>
			
			</footer>
			</div>
			
			
			<script type="text/javascript" src="/s/d41d8cd98f00b204e9800998ecf8427e-CDN/-6hucr7/802003/4460b9f1b01702e29f2e6191917088ae/1.0/_/download/batch/jira.webresources:bigpipe-js/jira.webresources:bigpipe-js.js" data-wrm-key="jira.webresources:bigpipe-js" data-wrm-batch-type="resource" data-initially-rendered=""></script>
			<script type="text/javascript" src="/s/d41d8cd98f00b204e9800998ecf8427e-CDN/-6hucr7/802003/4460b9f1b01702e29f2e6191917088ae/1.0/_/download/batch/jira.webresources:bigpipe-init/jira.webresources:bigpipe-init.js" data-wrm-key="jira.webresources:bigpipe-init" data-wrm-batch-type="resource" data-initially-rendered=""></script>
			
			<form id="jira_request_timing_info" class="dont-default-focus">
			<fieldset class="parameters hidden">
			<input type="hidden" title="jira.request.start.millis" value="1564235134315">
			<input type="hidden" title="jira.request.server.time" value="70">
			<input type="hidden" title="jira.request.id" value="1005x7263x1">
			<input type="hidden" title="jira.session.expiry.time" value="-">
			<input type="hidden" title="jira.session.expiry.in.mins" value="-">
			<input id="jiraConcurrentRequests" type="hidden" name="jira.request.concurrent.requests" value="1">
			<input type="hidden" title="db.conns.time.in.ms" value="0">
			</fieldset>
			</form>
			<!--
			REQUEST ID : 1005x7263x1
			REQUEST TIMESTAMP : [27/июл/2019:16:45:34 +0300]
			REQUEST TIME : 0,0700
			ASESSIONID : -
			CONCURRENT REQUESTS : 1
			
			db.conns : OpSnapshot{name='db.conns', invocationCount=2, elapsedTotal=374803, elapsedMin=92136, elapsedMax=282667, resultSetSize=0, cpuTotal=0, cpuMin=0, cpuMax=0}
			-->
			
			
			
			<div id="fancybox-tmp"></div><div id="fancybox-loading"><div></div></div><div id="fancybox-overlay"></div><div id="fancybox-wrap" class="fancybox-ie"><div id="fancybox-outer"><div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div><div id="fancybox-content"></div><a id="fancybox-close"></a><div id="fancybox-title"></div><a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a><a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a></div></div></body>`,
			name: '',
			listElement: [],
			pagePrev: false,
			step: 1,
			step2: '',
			step3: '',
			step4: '',
			step5: '',
			text2: '',
			text3: '',
			text4: '',
			text5: '',
			
		}
	},
	mounted() {
		let step=this
		
		let vm = document.getElementById('pagePrev');
		vm.addEventListener('click', function (e) {
			if(step.step == 2){step.step2 = 'input#login-form-username.text.medium-field'}
			if(step.step == 3){step.step3 = 'input#login-form-password.text.medium-field'}
			if(step.step == 4){step.step4 = 'input#login.aui-button.aui-button-primary'}
			if(step.step > 1){step.step = step.step + 1}
			e.preventDefault();
			
		});

	
	},
	methods: {
		openUrl(){
			// this.$http.get( `/OpenUrl?url=${this.siteurl}` ).then((res) => {
			// 	this.page = res.data.pageSource
			// })
			this.pagePrev=true
			this.step++;
			
		},
		getTypes(){
			this.$http.get( `/Types` ).then((res) => {
				console.log(res)
			})
		},
		saveTemplate(){
			this.step = 1;
			this.page = '<div  style="width:100%; line-height: 40px; background: #c11531; color: #fff; text-align: center;">Шаблон успешно сохранен</div>'
			// this.$http.post('/Templates', {
			// 	name: 'Авторизация в Jira для DevLeads',
			// 	elements: [
			// 		{
			// 			step: 1,
			// 			type: 1,
			// 			id: 'input#login-form-username.text.medium-field',
			// 			text: 'devlead.pro',

			// 		},
			// 		{
			// 			step: 2,
			// 			type: 1,
			// 			id: 'input#login-form-password.text.medium-field',
			// 			text: 'Ww84526758',
						
			// 		},
			// 		{
			// 			step: 3,
			// 			type: 3,
			// 			id: 'input#login.aui-button.aui-button-primary',
			// 			text: '',
						
			// 		},
			// 	]
			// })
			// .then(function (response) {
			// 	console.log(response);
			// })
			// .catch(function (error) {
			// 	console.log(error);
			// });
		},
		getTemplates(){
			this.$http.get('/Templates/1')
			.then(function (response) {
				console.log(response);
			})
			.catch(function (error) {
				console.log(error);
			});
		}
	},
	watch: {
		'$route'( to, from ) {
		},
		
		
	}
};
