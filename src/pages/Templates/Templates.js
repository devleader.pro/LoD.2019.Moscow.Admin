import SitePanel from '@components/SitePanel/SitePanel.vue'
export default {
	name: 'page',
	mixins: [  ],
	components: {
		SitePanel
	},
	mounted() {
		this.getTemplates()
	},
	props: {
		
	},
	data: function() {
		return {
			templates: [],
			templatesReady: false
			
		};
	},
	methods: {
	
		
		getTemplates(){
			this.$http.get( '/Templates/', {headers: {
				// Token: this.$root.token,
			}}).then( ( res ) => {
				this.templates = res.data;
				this.templatesReady = true;
			} )
			
		},
		
	
		// getTime(time){
		// 	const date = new Date(time);
		// 	const day = (date.getDate() < 10 ) ? '0' + date.getDate() : '' + date.getDate() 
		// 	const month = (date.getMonth() < 9 ) ? '0' + ( date.getMonth() + 1 ) : '' + ( date.getMonth() + 1 ) 
		// 	return `${day}.${month}.${date.getFullYear()}`
		// },
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
