import SitePanel from '@components/SitePanel/SitePanel.vue'

export default {
	name: 'page',
	mixins: [  ],
	components: {
		SitePanel,
	},
	mounted() {
		this.getDataExemple()
	},
	props: {
		
	},
	data: function() {
		return {
			data: [],
			dataReady: false,
			alltasks: [],
			page: ''
		};
	},
	methods: {
		getDataExemple(){
			// this.$http.get( 'https://board.devlead.pro/', {headers: {
			// 	Token: this.$root.token,
			// }}).then( ( res ) => {
			// 	this.data = res.data;
			// 	this.dataReady = true;
			// } )
			// this.$http.get( '/Task/', {headers: {
			// 	Token: this.$root.token,
			// }}).then( ( res ) => {

			// 	this.alltasks = res.data
				
			// } )

		},
		addUser(){
			this.$http.get( '/OpenUrl?url=https://board.devlead.pro/' ).then((res) => {
				this.page = res.data.pageSource
				console.log(res.data.pageSource, res) 
			})
		},
		getStat(uid){
			let countOk = 0, count = 0
			for (let index = 0; index < this.alltasks.length; index++) {
				const el = this.alltasks[index];
				if(el.userId == uid){
					count++;
					if(el.taskStatusId === 4){
						countOk++
					}
				}
			}
			return `${countOk}/${count}`
		}
	
	},
	watch: {
		'$route'( to, from ) {
		}
	}
};
